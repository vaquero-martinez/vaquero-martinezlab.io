---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Determinación Del Contenido Total de Electrones (TEC) de La Ionosfera En Extremadura
subtitle: ''
summary: ''
authors:
- Roc\ó García-Meléndez
- Mar\' ́Cruz Gallego
- Javier Vaquero-Mart\'iź
tags: []
categories: []
date: '2021-01-01'
lastmod: 2022-09-01T15:01:51+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:51.140569Z'
publication_types:
- '2'
abstract: ''
publication: '*Avances en Ciencias de la Tierra*'
---
