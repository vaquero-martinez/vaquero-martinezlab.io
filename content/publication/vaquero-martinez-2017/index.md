---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Validation of MODIS Integrated Water Vapor Product against Reference GPS Data
  at the Iberian Peninsula
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- José Pablo Ortiz de Galisteo
- Victoria E. Cachorro
- Maria João Costa
- Roberto Román
- Yasmine S. Bennouna
tags:
- Q1 Computers in Earth Sciences
- Q1 Earth-Surface Processes
- Q1 Global and Planetary Change
- Q1 Management; Monitoring; Policy and Law
categories: []
date: '2017-12-01'
lastmod: 2022-09-01T15:01:51+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:51.642159Z'
publication_types:
- '2'
abstract: ''
publication: '*International Journal of Applied Earth Observation and Geoinformation*'
doi: 10.1016/j.jag.2017.07.008
---
