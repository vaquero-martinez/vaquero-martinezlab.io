---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Did Anomalous Atmospheric Circulation Favor the Spread of COVID-19 in Europe?
subtitle: ''
summary: ''
authors:
- A. Sanchez-Lorenzo
- J. Vaquero-Martínez
- J. Calbó
- M. Wild
- A. Santurtún
- J.A. Lopez-Bustins
- J.M. Vaquero
- D. Folini
- M. Antón
tags:
- Q1 Biochemistry
- Q1 Environmental Science (miscellaneous)
categories: []
date: '2021-01-01'
lastmod: 2022-09-01T15:01:51+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:51.472804Z'
publication_types:
- '2'
abstract: ''
publication: '*Environmental Research*'
doi: 10.1016/j.envres.2020.110626
---
