---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Comparison of Long-Term Solar Radiation Trends from CM SAF Satellite Products
  with Ground-Based Data at the Iberian Peninsula for the Period 1985– 2015
subtitle: ''
summary: ''
authors:
- Javier Montero-Martín
- Manuel Antón
- Javier Vaquero-Mart\ńez
- Arturo Sanchez-Lorenzo
tags:
- Q1 Atmospheric Science
categories: []
date: '2020-05-01'
lastmod: 2022-09-01T15:01:51+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:51.290877Z'
publication_types:
- '2'
abstract: ''
publication: '*Atmospheric Research*'
doi: 10.1016/j.atmosres.2019.104839
---
