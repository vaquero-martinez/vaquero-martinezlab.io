---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Evaluation of Water Vapor Product from TROPOMI and GOME-2 Satellites against
  Ground-Based GNSS Data over Europe
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martinez
- Manuel Anton
- Ka Lok Chan
- Diego Loyola
tags: []
categories: []
date: '2022-07-01'
lastmod: 2022-09-01T15:01:53+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:53.200402Z'
publication_types:
- '2'
abstract: ''
publication: '*Atmosphere*'
doi: 10.3390/atmos13071079
---
