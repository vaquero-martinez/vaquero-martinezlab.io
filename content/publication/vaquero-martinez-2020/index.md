---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Water Vapor Satellite Products in the European Arctic: An Inter-Comparison
  against GNSS Data'
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- Roberto Román
- Victoria E. Cachorro
- Huiqun Wang
- Gonzalo González Abad
- Christoph Ritter
tags:
- bias:-2.67-5.87mm
- corr:0.47-0.85
- dateend:20171231
- datestart:20100101
- instrument:satellite:AIRS
- instrument:satellite:GOME2
- instrument:satellite:MODIS
- instrument:satellite:OMI
- instrument:satellite:POLDER
- instrument:satellite:SCIAMACHY
- nsta:1
- Q1 Environmental Chemistry
- Q1 Environmental Engineering
- Q1 Pollution
- Q1 Waste Management and Disposal
- region:European Arctic
- rmse:2.36-7.32mm
- type:validation
- what:satellite
categories: []
date: '2020-11-01'
lastmod: 2022-09-01T15:01:52+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:52.457343Z'
publication_types:
- '2'
abstract: ''
publication: '*Science of The Total Environment*'
doi: 10.1016/j.scitotenv.2020.140335
---
