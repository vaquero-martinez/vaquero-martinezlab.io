---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Recovery of Early Meteorological Records from Extremadura Region (SW Iberia):\
  \ The `CliPastExtrem' (v1.0) Database"
subtitle: ''
summary: ''
authors:
- José M. Vaquero
- Nieves Bravo-Paredes
- María Angeles Obregón
- V̧́tor M. S. Carrasco
- Maria Antonia Valente
- Ricardo M. Trigo
- Fernando Dom\'ǵuez-Castro
- Montero-Mart\'i ́Javier
- ́ Javier Vaquero-Mart\'in
- Manuel Antón
- ósé Agust\'in a ́Gallego, Mar\'ia Cr Garc\'ia
tags: []
categories: []
date: '2021-09-01'
lastmod: 2022-09-01T15:01:53+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:53.529954Z'
publication_types:
- '2'
abstract: ''
publication: '*Geoscience Data Journal*'
doi: 10.1002/gdj3.131
---
