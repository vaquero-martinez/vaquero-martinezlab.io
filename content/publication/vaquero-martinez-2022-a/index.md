---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Comparison of CIMEL Sun-Photometer and Ground-Based GNSS Integrated Water Vapor
  over South-Western European Sites
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- André F. Bagorrilha
- Manuel Antón
- Juan C. Antuña-Marrero
- Victoria E. Cachorro
tags: []
categories: []
date: '2022-09-01'
lastmod: 2022-09-01T15:01:53+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:53.068631Z'
publication_types:
- '2'
abstract: ''
publication: '*Atmospheric Research*'
doi: 10.1016/j.atmosres.2022.106217
---
