---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Water Vapor Radiative Effects on Short-Wave Radiation in Spain
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- José Pablo Ortiz de Galisteo
- Roberto Román
- Victoria E. Cachorro
tags:
- Q1 Atmospheric Science
- type:other
categories: []
date: '2018-06-01'
lastmod: 2022-09-01T15:01:52+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:52.149588Z'
publication_types:
- '2'
abstract: ''
publication: '*Atmospheric Research*'
doi: 10.1016/j.atmosres.2018.02.001
---
