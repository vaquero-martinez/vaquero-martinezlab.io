---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Integrated Water Vapor over the Arctic: Comparison between Radiosondes and
  Sun Photometer Observations'
subtitle: ''
summary: ''
authors:
- Juan Carlos Antuña-Marrero
- Roberto Román
- Victoria E. Cachorro
- David Mateos
- Carlos Toledano
- Abel Calle
- Juan Carlos Antuña-Sánchez
- Javier Vaquero-Martínez
- Manuel Antón
- Ángel M. de Frutos Baraja
tags: []
categories: []
date: '2022-02-01'
lastmod: 2022-09-01T15:01:50+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:01:49.178775Z'
publication_types:
- '2'
abstract: ''
publication: '*Atmospheric Research*'
doi: 10.1016/j.atmosres.2022.106059
---
