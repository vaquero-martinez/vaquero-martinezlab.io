---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Validation of Integrated Water Vapor from OMI Satellite Instrument against
  Reference GPS Data at the Iberian Peninsula
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- José Pablo Ortiz de Galisteo
- Victoria E. Cachorro
- Huiqun Wang
- Gonzalo González Abad
- Roberto Román
- Maria João Costa
tags:
- bias:-0.3mm
- corr:0.79
- country:Spain
- dateend:20091231
- datestart:20070101
- include:no
- instrument:satellite:OMI
- nsta:9
- Q1 Environmental Chemistry
- Q1 Environmental Engineering
- Q1 Pollution
- Q1 Waste Management and Disposal
- region:Iberian Peninsula
- rmse:5.1(IQR)mm
- what:satellite:OMI
categories: []
date: '2017-02-01'
lastmod: 2022-09-01T15:02:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:02:21.138580Z'
publication_types:
- '2'
abstract: ''
publication: '*Science of The Total Environment*'
doi: 10.1016/j.scitotenv.2016.12.032
---
