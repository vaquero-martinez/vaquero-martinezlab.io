---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Inter-Comparison of Integrated Water Vapor from Satellite Instruments Using
  Reference GPS Data at the Iberian Peninsula
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- José Pablo Ortiz de Galisteo
- Victoria E. Cachorro
- Pablo Álvarez-Zapatero
- Roberto Román
- Diego Loyola
- Maria João Costa
- Huiquin Wang
- Gonzalo González Abad
- Stefan Noël
tags: []
categories: []
date: '2018-01-01'
lastmod: 2022-09-01T15:02:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:02:21.365903Z'
publication_types:
- '2'
abstract: ''
publication: '*Remote Sensing of Environment*'
doi: 10.1016/j.rse.2017.09.028
---
