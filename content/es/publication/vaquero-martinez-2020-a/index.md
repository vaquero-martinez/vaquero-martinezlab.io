---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Evaluation of Water Vapor Radiative Effects Using GPS Data Series over Southwestern
  Europe
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- Arturo Sanchez-Lorenzo
- Victoria E. Cachorro
tags:
- Q1 Earth and Planetary Sciences (miscellaneous)
- type:other
categories: []
date: '2020-04-01'
lastmod: 2022-09-01T15:02:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:02:22.050602Z'
publication_types:
- '2'
abstract: ''
publication: '*Remote Sensing*'
doi: 10.3390/rs12081307
---
