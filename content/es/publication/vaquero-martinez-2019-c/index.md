---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Comparison of Integrated Water Vapor from GNSS and Radiosounding at Four GRUAN
  Stations
subtitle: ''
summary: ''
authors:
- Javier Vaquero-Martínez
- Manuel Antón
- José Pablo Ortiz de Galisteo
- Roberto Román
- Victoria E. Cachorro
- David Mateos
tags:
- bias:-0.87-(-0.49)mm
- dateend:20180122
- datestart:20060521
- instrument:radiosonde
- Q1 Environmental Chemistry
- Q1 Environmental Engineering
- Q1 Pollution
- Q1 Waste Management and Disposal
- region:global
- stdev:0.61-1.10mm
- what:gnss
categories: []
date: '2019-01-01'
lastmod: 2022-09-01T15:02:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-09-01T13:02:21.711989Z'
publication_types:
- '2'
abstract: ''
publication: '*Science of The Total Environment*'
doi: 10.1016/j.scitotenv.2018.08.192
---
