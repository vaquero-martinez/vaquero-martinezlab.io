---
title: ¡Hola, Mundo!
author: J. Vaquero-Martínez
date: '2020-08-08'
slug: hola-mundo
categories:
  - other
tags:
  - hello world
subtitle: ''
summary: ''
authors: []
lastmod: '2020-08-08T21:52:39+02:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Hola a todos, este es mi primer post. Es básicamente una prueba de que todo funciona bien.

Espero mantener esta página activa y añadir mucho contenido.

¡Salud!
