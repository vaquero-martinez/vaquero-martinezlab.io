---
title: Resumen de física para selectividad
author: J. Vaquero-Martínez
date: '2020-08-11'
slug: resumen-de-física-para-selectividad
categories:
  - Docencia
tags:
  - Material Académico
summary: 'Hace poco hice un libro tipo `bookdown` con unos apuntes esquemáticos para la preparación del examen de selectividad de Física (particularmente de Extremadura). Incluye los temas de ondas y gravitación, electromagnetismo, óptica, y física moderna. Está escrito en castellano y puede descargarse como epub o pdf; también puede verse online. Es mi primer libro `bookdown`.'
authors: []
external_link: 'https://vaquero-martinez.gitlab.io/resumen-de-fisica-para-selectividad'
image:
  caption: ''
  focal_point: ''
  preview_only: no
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
slides: ''
---
