---
authors:
- admin
bio: Mis intereses incluyen la didáctica de las ciencias de la tierra y de la física, el balance radiativo del vapor de agua, productos de vapor de agua de GNSS/GPS, y otras ténicas de teledetección especialmente enfocadas en la medida de vapor de agua.
education:
  courses:
  - course: Doctor por la Universidad de Extremadura
    Institution: Universidad de Extremadura
    year: 2021
  - course: Máster de Investigación en Ciencias (especialidad Física)
    Institution: Universidad de Extremadura
    year: 2016
  - course: Máster Universitario de Formación del Profesorado de Educación Secundaria (especialidad Física y Química)
    Institution: Universidad de Extremadura
    year: 2015  
  - course: Grado en Física
    institution: Universidad de Extremadura
    year: 2013
email: "javier_vm@unex.es"
interests:
- Didáctica de las ciencias de la tierra y de la física
- Vapor de agua.
- Óptica atmosférica.
- Transferencia radiativa.
- Meteorología GPS/GNSS.
- Teledtección del vapor de agua
- \#Rstats.
organizations:
- name: Universidade de Vigo
  url: "https://uvigo.es"
role: Investigador postdoctoral
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:javier_vm@unex.es'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/Javi_VM
- icon: ResearchGate
  icon_pack: fab
  link: https://www.researchgate.net/profile/Javier_Vaquero-Martinez
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/vaquero-martinez
superuser: true
title: Javier Vaquero-Martínez
user_groups:
- Researchers
- Visitors
---

Mi nombre es Javier Vaquero-Martínez y soy profesor contratado doctor en la Universidad de Extremadura. Soy un entusiasta del lenguaje R, y también juego al ajedrez. Tengo aficiones de todo tipo, como la literatura, el pádel, GNU/Linux y el Software Libre. 
