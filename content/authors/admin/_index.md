---
authors:
- admin
bio: My research interests include teaching on earth science, water vapor radiative balance, GNSS/GPS water vapor products, and other remote sensing techniques specially focused in water vapor measuring.
education:
  courses:
  - course: PhD in Atmospheric Science
    Institution: Universidad de Extremadura
    year: 2021
  - course: Master in Research in Science (Physics)
    Institution: Universidad de Extremadura
    year: 2016
  - course: Master in High School Teaching (Physics and Chemistry)
    Institution: Universidad de Extremadura
    year: 2015  
  - course: Physics Degree
    institution: Universidad de Extremadura
    year: 2013
email: "javier_vm@unex.es"
interests:
- Teaching on Earth Science and Physics.
- Water vapor.
- Atmospheric optics.
- Radiative transfer.
- GPS/GNSS meteorology.
- Remote sensing of water vapor.
- Rstats.
organizations:
- name: Universidad de Extremadura
  url: "https://www.unex.es"
role: Associate Professor.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:javier_vm@unex.es'
- icon: mastodon
  icon_pack: fab
  link: https://fediscience.org/@javi_vm
- icon: ResearchGate
  icon_pack: fab
  link: https://www.researchgate.net/profile/Javier_Vaquero-Martinez
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/vaquero-martinez
superuser: true
title: Javier Vaquero-Martínez
user_groups:
- Researchers
- Visitors
---

My name is Javier Vaquero-Martínez. I got my PhD at Department of Physics in Universidad de Extremadura in Badajoz, Spain. I am currently associate professor at the same university. I am an R enthusiast, and also a chess player. I also like other sorts of stuff, like literature, padel, GNU/Linux and free software.
