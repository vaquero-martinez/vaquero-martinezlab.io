---
title: Summary of physics contents in Selectivity exam
author: J. Vaquero-Martínez
date: '2020-08-11'
slug: summary-of-physics-contents-in-selectivity-exam
categories:
  - Teaching
tags:
  - Teaching material
summary: 'This is a bookdown book I did with some schematic notes to prepare the Physics “access to university” exam in Spain (particularly in Extremadura). It includes Waves and Gravity, Electromagnetism, Optics, and Modern Physics. It is written in Spanish. It is my first bookdown book!'
authors: []
external_link: 'https://vaquero-martinez.gitlab.io/resumen-de-fisica-para-selectividad'
image:
  caption: ''
  focal_point: ''
  preview_only: no
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
slides: ''
---
